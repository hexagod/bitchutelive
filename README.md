# BitchuteLIVE

The decentralized bitchute live streaming protocol. Built and powered by the bitchute community. The entire project will be open source and use open-source components from day one so any code you contribute our use must comply with that.

Mini Project 1: How you can help with the live streaming page template.
We're primarily listing the visual elements, more of the backend work will be available later for those interested.
We have selected bootstrap 4 to structure the template as it seems to work well and lots of people are familiar with working with it.

Streaming page
- Make it look and feel great
- Add a slide out menu with useful options
- Night and day modes
- Add modals for logging in and other relevant use cases
- Create a backend page for the Streamer to administrate the stream

Chat widget
- Add some professional styling
- Add profile images
- Add emojis and possibly stickers
- Add buttons for different modes
- Any other things we're missing from chat?
- Create a back end page template for the streamerto administer chats 
(Note the chats are currently using a decentralized database called GUN https://gun.eco)

Super chat widget
- Add the visual elements, creating a chat and the display and javascript timers to expire the chats (we can progress the backend including payments processing later on)
- Create a backend page for the streamer to administer chats

If you are interested in contributing and would like to reach us please use volunteer@bitchute.com
We're also bringing on a new community manager this first of March who will be amoung other things involved in running a Telegram group and coordinating efforts.